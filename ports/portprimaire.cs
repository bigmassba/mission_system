namespace TodoApi;
using System.Text.Json;
public interface Portprimaire
{
   JsonElement GetPertype(uint personid, TypeDeConsultant tdc);

}
public enum TypeDeConsultant
    {
        Devops,
        Sysops,
        FullstackDev
    }
public enum PersonnelleAfd
    {
        Consultant,
        Commerciale,
        Administration
    }