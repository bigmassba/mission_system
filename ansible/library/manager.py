#!/usr/bin/env python3

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: manage-service

short_description: This is for managing all host service

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: This module is for managing all host services with filter.

options:
    osSystem:
        description: It is the systeme we need to manage.
        required: true
        type: str
    commandeT:
        description: It is action to do.
        required: true
        type: str
    filterO:
        description: It is for cammande output filter option.
        required: false
        type: str
    servicesC:
        description: It is the service to manage.
        required: false
        type: str
    reloadS:
        description: It is if need to save service modify.
        required: false
        type: bool
# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
# extends_documentation_fragment:
#     - my_namespace.my_collection.my_doc_fragment_name

author:
    - Massire(bigmassba@gmail.com)
'''

EXAMPLES = r'''
# Pass name to the module 
- name: Liste or manage all or any services
  my_namespace.my_collection.manage-service:
  #system paramter most take sysV or systemd
    system: sysV
  # commande can list/state/enable/desable/start/stop/restart/reload
    commande:list
  # all option parameter for this module
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
message:
    description: The output message that the manage module generates.
    type: str
    returned: always
    sample: ''
'''

from ansible.module_utils.basic import AnsibleModule
spsystem=["sysV","systemd"]
spcommandV=["--status-all","stop","start","restart"]
spcommandD=["list-units","status","enable","disable","stop","start","restart","reload","daemon-reload"]
'''
sysV service commande

Commande	             Fonction
service --status-all	 Lister les services
service [nom du service] stop	Arreter un service
service [nom du service] start	Demarrer un service
service [nom du service] restart	Redemarrer un service


Systemd systemectl commande

Commande	                                Fonction
systemctl list-units	                    Lister les services
systemctl status [nom du service].service	Connaitre l'etat d'un service
systemctl enable [nom du service].service	Activer un service
systemctl disable [nom du service].service	Desactiver un service
systemctl stop [nom du service].service	    Arreter un service
systemctl start [nom du service].service	Demarrer un service
systemctl restart [nom du service].service	Redemarrer un service
systemctl reload [nom du service].service	Recharge du service avec les nouveaux parametres (evite les coupures)
systemctl daemon-reload
'''

{
    "ANSIBLE_MODULE_ARGS": {
        "osSystem": "hello",
        "commandeT":"list",
        "filterO":"",
        "servicesC":"",
        "reloadS":False
        
    }
}
def main():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        osSystem=dict(type='str', required=True),
        commandeT=dict(type='str', required=True),
        filterO=dict(type='str', required=False),
        servicesC=dict(type='str', required=False),
        reloadS=dict(type='bool', required=False),
    )
    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        message=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    for i in range(len(spcommandD)):
         for j in range(len(spcommandV)): 
              for k in range(len(spsystem)):
                if (module.params['osSystem'] != spsystem[k] or module.params['commandeT'] != spcommandV[j] or module.params['commandeT'] != spcommandD[i]  ):
                    result['message'] = 'Outch !!!'
                    result['message'] = 'voila %s %s %s' %(spsystem[i],spcommandV[j],spcommandD[k])
                    #result['message'] = '%s %s %s.service' % (module.params['osSystem'],module.params['commandeT'],module.params['servicesC'])    
                    module.fail_json(msg='The module Failed', **result)
              else:
                   if (module.params['commandeT'] == 'list'):
                      result['changed'] = True
                      result['message'] = '%s %s' %module.params['osSystem'] %module.params['commandeT']
                   else:
                      if module.params['osSystem'] =='sysV':
                         result['changed'] = True
                         result['message'] = '%s %s %s' %module.params['osSystem'] %module.params['servicesC'] %module.params['commandeT']
                      else:
                         result['changed'] = True
                         result['message'] = '%s %s %s.service' %module.params['osSystem'] %module.params['commandeT'] %module.params['servicesC']    
    # use whatever logic you need to determine whether or not this module
    # made any modifications to your target


    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)

if __name__ == '__main__':
    main()
