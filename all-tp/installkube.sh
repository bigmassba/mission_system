#!/bin/bash





sudo apt-get update && sudo apt-get install -y apt-transport-https ca-certificates curl

sudo mkdir -p /etc/apt/keyrings

sudo rm -f /etc/apt/keyrings/kubernetes-archive-keyring.gpg

curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-archive-keyring.gpg

echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list



sudo apt-get update

sudo apt-get install -y kubelet kubeadm kubectl ipvsadm

sudo apt-mark hold kubelet kubeadm kubectl

