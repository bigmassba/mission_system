namespace TodoApi;
public class ProfileEvaluator : IProfileEvaluator
{
    private const double sliceCountPerProfile = 6;

    private IProfileMatch profileMatch;

    public ProfileEvaluator(IProfileMatch profileMatch)
    {
        this.profileMatch = profileMatch;
    }

    public int GetProfileCount(uint personCount, ProfileKind profileKind)
    {
        var profileStats = this.profileMatch.GetProfileStatistics();
        var foundProfileStat = profileStats.FirstOrDefault(s => s.ProfileKind.Equals(profileKind));
        if (foundProfileStat == null)
        {
            throw new InvalidOperationException("Type de profile inconnu");
        }
        double profileCount = ((double)foundProfileStat.AverageSliceCount * personCount)/sliceCountPerProfile;
        return (int)Math.Ceiling(profileCount);
    }
}