namespace TodoApi;

public interface IProfileEvaluator
{
   int GetProfileCount(uint personCount, ProfileKind profileKind);

}
public enum ProfileKind
    {
        Devops,
        Sysops,
        FullstackDev
    }