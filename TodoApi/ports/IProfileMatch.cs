namespace TodoApi;

public interface IProfileMatch
{
     IEnumerable<ProfileStat> GetProfileStatistics();
}

public class ProfileStat
{
    public ProfileKind ProfileKind { get; set; }
    public int  AverageSliceCount { get; set; }
}