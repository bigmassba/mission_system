namespace TodoApi;

public interface Portsecondaire
{
     IEnumerable<Persontype> GetPersonneInfo();
}

public class Persontype
{
    public TypeDeConsultant tdc { get; set; }
    public int  Personid { get; set; }
}