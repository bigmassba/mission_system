// using System;
// using System.Linq;

// namespace TodoApi
// {
//     public class ProfileEvaluator : IProfileEvaluator
//     {
//         private const double sliceCountPerProfile = 2;
//         private IProfileRepository profileRepository;        
//         // private string  Profile ;
//         // private int  Evaluation ;
//         // private int  skill  ;
//         public ProfileEvaluator(IProfileRepository profileRepository)
//         {
//             this.profileRepository = profileRepository;
//             Profile = "Devops" ;
//             Evaluation = 70;
//             skill = 10 ;
//         }

//         public int GetProfileCount(uint personCount, ProfileKind profileKind)
//         {
//             var profileStats = this.profileRepository.GetProfileStatistics();
//             var foundProfileStat = profileStats.FirstOrDefault(s => s.ProfileKind.Equals(profileKind));
//             if (foundProfileStat == null)
//             {
//                 throw new InvalidOperationException("Type de profile inconnu");
//             }

//             double profileCount = ((double)foundProfileStat.AverageSliceCount * personCount)/sliceCountPerProfile;
//             return (int)Math.Ceiling(profileCount);
//         }
//     }
// }