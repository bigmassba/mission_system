using TodoApi;
using System.Text.Json;

public class Domaine : Portprimaire
{
    private const double sliceCountPerPizza = 6;

    private Portsecondaire ps;

    public Domaine(Portsecondaire ps)
    {
        this.ps = ps;
    }

    public JsonElement GetPertype(uint personCount, TypeDeConsultant perstype)
    {
        var personinfos = this.ps.GetPersonneInfo();
        var foundStat = personinfos.FirstOrDefault(s => s.tdc.Equals(perstype));
        if (foundStat == null)
        {
            throw new InvalidOperationException("Type de PROFILE inconnu");
        }
        string data = @" [ {""name"": ""John Doe"", ""occupation"": ""gardener""}, 
         {""name"": ""Peter Novak"", ""occupation"": ""driver""} ]";
        using JsonDocument doc = JsonDocument.Parse(data);
         JsonElement userInfos = doc.RootElement;
       
        return userInfos;
    }
}