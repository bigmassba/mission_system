using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

namespace TodoApi.Controllers;

[ApiController]
[Route("[controller]/{id:int}")]
public class DomaineController : ControllerBase
{

    private static readonly string[] Summaries = new[]
    {
        "DevOps is coll", "But Sys is boolcheet",  "And fullStack is better"
    };
    private readonly ILogger<DomaineController> _logger;

    public DomaineController(ILogger<DomaineController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "GetDomaineController")]
    public    UserinfoTYpe Get(int id)
    {
    var r = new AdapterSecondaire();
    var p = new Domaine(r);
    var c = new AdapterPrimaire(p);
    return  new UserinfoTYpe
        {
            BeginDate = DateTime.Today,
            PositionName = c.LaunchCalculation(id),
            MatchScore = id,
            Description = Summaries[id-1]
        };     
}
    
    [HttpPost(Name = "PostDomaineController")]
     public void Post(int id, string description)
      {
        Summaries[id-1] = description;
    }
}

