
using System.Text.Json;
using TodoApi;

public class AdapterPrimaire
{
    private Portprimaire ps;

    public AdapterPrimaire(Portprimaire ps )
    {
        this.ps = ps;
    }
 
    public string LaunchCalculation(int id)
    {
      
       // uint personCount = 1;
        
        int ct = id;

        TypeDeConsultant p;
        switch (ct)
        {
            case 1:
                p = TypeDeConsultant.Devops;
                break;
            case 2:
                p = TypeDeConsultant.Sysops;
                break;
            case 3:
                p = TypeDeConsultant.FullstackDev;
                break;
            default:
               string data = @"  [{""name"": ""John Doe"", ""occupation"": ""gardener""}, 
               {""name"": ""Peter Novak"", ""occupation"": ""driver""} ]";
               JsonDocument doc = JsonDocument.Parse(data);
               JsonElement userInfos = doc.RootElement;
              return  userInfos[0].ToString();
        }

         return  p.ToString()  ;
       
    }
}

public class UserinfoTYpe
{
    public DateTime BeginDate { get; set; }

    public string? PositionName { get; set; }

    public int MatchScore  { get; set; }
    public string? Description { get; set; }
}
