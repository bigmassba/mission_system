namespace TodoApi;
public class AdapterSecondaire : Portsecondaire
{
    private readonly List<Persontype> persontype = new List<Persontype>{
            new Persontype { tdc = TypeDeConsultant.Devops, Personid = 1},
            new Persontype { tdc = TypeDeConsultant.Devops, Personid = 2},
            new Persontype { tdc = TypeDeConsultant.Devops, Personid = 3}
        };

    public IEnumerable<Persontype> GetPersonneInfo()
    {
        return this.persontype;
    }
}