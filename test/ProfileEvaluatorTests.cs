using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Profile.Evaluator;
using System.Collections.Generic;

namespace profile_calculator_tests
{
    [TestClass]
    public class ProfileCalculatorTests
    {
        private Mock<IProfileRepository> repositoryMock;

        [TestInitialize]
        public void Initialize()
        {
            this.repositoryMock = new Mock<IProfileRepository>();
            var profileStats = new List<ProfileStat>{
                new ProfileStat { ProfileKind = ProfileKind.Regina, AverageSliceCount = 6},
                new ProfileStat { ProfileKind = ProfileKind.Pepperoni, AverageSliceCount = 4},
                new ProfileStat { ProfileKind = ProfileKind.Vegetarian, AverageSliceCount = 7},
            };

            this.repositoryMock.Setup(r => r.GetProfileStatistics()).Returns(profileStats);
        }

        [TestMethod]
        public void When_Requesting_ProfileCount_For_One_Person_Then_ProfileCalculator_Shall_Return_Proper_Profile_Count()
        {
            var profileCalculator = new ProfileCalculator(this.repositoryMock.Object);
            
            int profileCount = profileCalculator.GetProfileCount(1, ProfileKind.Regina);
            Assert.AreEqual(1, profileCount);
            profileCount = profileCalculator.GetProfileCount(1, ProfileKind.Pepperoni);
            Assert.AreEqual(1, profileCount);
            profileCount = profileCalculator.GetProfileCount(1, ProfileKind.Vegetarian);
            Assert.AreEqual(2, profileCount);
        }

        [TestMethod]
        public void When_Requesting_ProfileCount_For_Several_Persons_Then_ProfileCalculator_Shall_Return_Proper_Profile_Count()
        {
            var profileCalculator = new ProfileCalculator(this.repositoryMock.Object);
            
            int profileCount = profileCalculator.GetProfileCount(3, ProfileKind.Regina);
            Assert.AreEqual(3, profileCount);
            profileCount = profileCalculator.GetProfileCount(3, ProfileKind.Pepperoni);
            Assert.AreEqual(2, profileCount);
            profileCount = profileCalculator.GetProfileCount(3, ProfileKind.Vegetarian);
            Assert.AreEqual(4, profileCount);
        }
    }
}
